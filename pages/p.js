import { useEffect, useState } from "react";
import Chart from "../components/chart";
import dayjs from "dayjs";
import useRealtime from "../hooks/useRealtime";
// random data
const random = (i) => Number(((Math.random() * 10) / i).toFixed());
/**
 * visualisasi data rest api 3 array dengan object
 * date : tanggal
 * data : integer value
 */
const data = Array.from(Array(3).keys()).map((i) => ({
  date: new Date(Date.now() - 1000 * 10 * i).toISOString(),
  data: random(2),
}));

data.reverse();

const data2 = Array.from(Array(3).keys()).map((i) => ({
  date: new Date(Date.now() - 1000 * 10 * i).toISOString(),
  data: random(3),
}));

data2.reverse();
//

function App() {
  // simulasi get chart dari rest api
  const [chart, setChart] = useState(data);
  const [chart2, setChart2] = useState(data2);
  // simulasi get chart realtime socket
  const latestData = useRealtime();
  const latestData2 = useRealtime();
  // listener ketika 10 detik maka menambah data chart
  useEffect(() => {
    if (latestData.data) {
      const date1 = dayjs(latestData.date);
      const date2 = date1.diff(dayjs(chart[chart.length - 1].date), "second");

      if (date2 > 9) {
        setChart((prev) => [...prev, latestData]);
        setChart2((prev) => [...prev, latestData2]);
      }
    }
  }, [latestData.date, chart.length, latestData2.date]);
  return (
    <div
      style={{
        display: "flex",
        flexDirection: "column",
        width: "100vw",
        alignItems: "center",
      }}
    >
      {/* mock responve socket */}
      <p>last data : {JSON.stringify(latestData)}</p>
      <p>last data : {JSON.stringify(latestData2)}</p>
      <div style={{ width: "50vw" }}>
        <Chart
          label="data1"
          data={[
            ...chart.map((i) => ({
              data: i.data,
              label: dayjs(i.date).format("DD/MM HH:mm:ss"),
            })),
            {
              data: latestData.data,
              label: dayjs(latestData.date).format("DD/MM HH:mm:ss"),
            },
          ]}
        />
      </div>
      <div style={{ width: "50vw" }}>
        <Chart
          label="data3"
          data={[
            ...chart2.map((i) => ({
              data: i.data,
              label: dayjs(i.date).format("DD/MM HH:mm:ss"),
            })),
            {
              data: latestData2.data,
              label: dayjs(latestData2.date).format("DD/MM HH:mm:ss"),
            },
          ]}
        />
      </div>
    </div>
  );
}

export default App;
