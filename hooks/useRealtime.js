import { useEffect, useState } from "react";

const random = () => Number((Math.random() * 10).toFixed());

function useRealtime() {
  const [data, setData] = useState(50);
  useEffect(() => {
    const interval = setInterval(() => {
      setData(random());
    }, 1000);

    return () => {
      clearInterval(interval);
    };
  }, [setData]);

  return { date: new Date().toISOString(), data };
}

export default useRealtime;
