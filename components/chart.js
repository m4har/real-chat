import { Line } from "react-chartjs-2";

const options = {
  scales: {
    yAxes: [
      {
        ticks: {
          beginAtZero: true,
        },
      },
    ],
  },
  bezierCurve: false,
};

/**
 *
 * @param {{
 *  data:{data:string,label:string}[],
 *  label:string
 * }} props
 * @returns
 */
function Chart(props) {
  const { data, label } = props;

  const dataChart = {
    labels: data.map((i) => i.label),
    datasets: [
      {
        label: label,
        data: data.map((i) => i.data),
        fill: false,
        lineTension: 0.2,
        backgroundColor: "rgb(255, 99, 132)",
        borderColor: "rgba(255, 99, 132, 0.5)",
      },
    ],
  };

  return <Line data={dataChart} options={options} />;
}

Chart.defaultProps = {
  data: [],
  label: "",
};

export default Chart;
